#!/usr/bin/env bash

semversioner add-change --type patch --description "$@"
git add .changes
git commit -a -m "$@"

