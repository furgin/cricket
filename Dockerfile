FROM node:12

COPY package.json /opt/service/
WORKDIR /opt/service
RUN yarn install

COPY . /opt/service/

EXPOSE 8080

ENTRYPOINT ["node", "src/index.js"]
