#!/usr/bin/env bash

server_version=$(semversioner current-version)
image="docker.atl-paas.net/${USER}/cricket-bot"

function update_version() {
  new_version=$1
  yarn version --new-version "${new_version}" --no-git-tag-version

  git add .
  git commit -m "Update files for new version '${new_version}' [skip ci]"
  git push origin

  git tag -a -m "Tagging for release ${new_version}" "${new_version}"
  git push origin "${new_version}"
}

function build_docker() {
  new_version=$1
  docker build -t "${image}" .
  docker tag "${image}" "${image}:${new_version}"
  docker push "${image}"
}

function micros_release() {
  new_version=$1
  DOCKER_IMAGE="${image}" DOCKER_TAG="${new_version}" \
    atlas micros service deploy \
    --service="mjensen-cricket-bot" \
    --env=ddev -f service-descriptor.yml
}

semversioner release
new_version=$(semversioner current-version)

if [[ "${server_version}" != "${new_version}" ]]; then
  update_version "${new_version}"
  build_docker "${new_version}"
  micros_release "${new_version}"
fi


