const espncricket = require('./espncricket');


espncricket.matches()
    .then((matches) => {
        matches.forEach(m =>
            espncricket
                .match(m.series_id, m.unique_id)
                .then(m => {

                    const diff = m.start_datetime_gmt.diffNow();

                    if (Math.abs(diff.as("days")) < 3) {

                        console.log(m.series_id);
                        console.log(m.unique_id);
                        console.log(m.title);
                        // console.log(m.start_datetime_gmt+" ("+i.length('days')+")");
                        console.log(diff.as("days"));
                        console.log(`${m.series.short_name}: ${m.title}`);
                        m.teams.forEach((team) => {
                            console.log(team.name);
                            team.innings.forEach(inn => console.log(inn.description));
                        });
                        console.log(m.result_description);
                        console.log();

                    }

                }));

    });