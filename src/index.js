require('dotenv').config()

const {App} = require('@slack/bolt')
    , espncricket = require('./espncricket');

const app = new App({
    signingSecret: process.env.SLACK_SIGNING_SECRET,
    token: process.env.SLACK_BOT_TOKEN,
});

const slashCommand = process.env.NODE_ENV === "production" ? "/cricket" : `/cricket-${process.env.USER}`;
console.log(`Using command: ${slashCommand}`);

app.command(slashCommand,
    async ({
               command,
               ack,
               say,
               respond
           }) => {
        await ack();

        const parts = command.text.split(" ");
        const cmd = parts[0] || "help";

        switch (cmd) {

            case 'matches':
                let filter = parts[1];

                await respond({
                    "response_type": "ephemeral",
                    "blocks": [
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": `You said \`${slashCommand} ${command.text}\``
                            },
                        }
                    ]
                });

                espncricket.matches()
                    .then(async matches => {
                        Promise.all(matches.map((match) => espncricket.match(match.series_id, match.unique_id)))
                            .then(async results => {

                                const filtered = results.filter(result =>
                                    !filter ||
                                    result.series.short_name.toLowerCase().includes(filter.toLowerCase()) ||
                                    result.series.name.toLowerCase().includes(filter.toLowerCase()) ||
                                    result.teams.map(team => team.name).join(" ").toLowerCase().includes(filter.toLowerCase()) ||
                                    result.teams.map(team => team.short_name).join(" ").toLowerCase().includes(filter.toLowerCase())
                                )
                                    .filter(result => Math.abs(result.start_datetime_gmt.diffNow().as("days")) < 3);

                                if (filtered.length) {

                                    const blocks = filtered
                                        .map(result => {
                                            return [
                                                {
                                                    "type": "context",
                                                    "elements": [
                                                        {
                                                            "type": "mrkdwn",
                                                            "text": "" + result.unique_id
                                                        },
                                                        {
                                                            "type": "image",
                                                            "image_url": result.teams[0].logo,
                                                            "alt_text": result.teams[0].name
                                                        },
                                                        {
                                                            "type": "plain_text",
                                                            "text": " "
                                                        },
                                                        {
                                                            "type": "plain_text",
                                                            "text": "vs"
                                                        },
                                                        {
                                                            "type": "image",
                                                            "image_url": result.teams[1].logo,
                                                            "alt_text": result.teams[1].name
                                                        },
                                                        {
                                                            "type": "plain_text",
                                                            "text": " "
                                                        },
                                                        {
                                                            "type": "mrkdwn",
                                                            "text": result.result_description
                                                        }
                                                    ]
                                                },
                                            ]

                                        })
                                        .reduce((acc, cur) => acc.concat(cur), []);

                                    // TODO: Not sure why this isn't working
                                    // await respond({
                                    //     "delete_original": "true"
                                    // });

                                    // useful for debugging
                                    // console.log(JSON.stringify(blocks, null, 2))

                                    await respond({
                                        "response_type": "ephemeral",
                                        // "replace_original": true,
                                        "blocks": blocks
                                            .filter((_, i) => i < 50)
                                    });
                                } else {
                                    await respond({
                                        "response_type": "ephemeral",
                                        "replace_original": true,
                                        "text": "No matches found."
                                    });

                                }
                            })

                    });
                break;

            case 'score':
                let unique_id = parseInt(parts[1]);

                await respond({
                    "response_type": "ephemeral",
                    "blocks": [
                        {
                            "type": "section",
                            "text": {
                                "type": "mrkdwn",
                                "text": `You said \`${slashCommand} ${command.text}\``
                            },
                        }
                    ]
                });

                espncricket.matches()
                    .then(matches => {
                        const match = matches.find(match => match.unique_id === unique_id);
                        espncricket.match(match.series_id, unique_id)
                            .then(async (m) => {
                                const blocks = m.teams.map(team => {
                                    return {
                                        "type": "section",
                                        "text": {
                                            "type": "mrkdwn",
                                            "text": `*${team.name}*\n${team.score_description}`
                                        },
                                        "accessory": {
                                            "type": "image",
                                            "image_url": team.logo,
                                            "alt_text": team.name
                                        }
                                    }
                                });
                                blocks.push(
                                    {
                                        "type": "section",
                                        "text": {
                                            "type": "mrkdwn",
                                            "text": m.result_description
                                        }
                                    });
                                await respond({
                                    "response_type": "ephemeral",
                                    "replace_original": true,
                                    "blocks": blocks
                                });
                            })
                    });
                break;

            default:
            case 'help':

                await respond(
                    {
                        "response_type": "ephemeral",
                        "blocks": [
                            {
                                "type": "section",
                                "text": {
                                    "type": "mrkdwn",
                                    "text": `Usage: ${slashCommand} COMMAND [OPTIONS]`
                                },
                                "fields": [
                                    {"type": "mrkdwn", "text": `*${slashCommand} matches [TEAM|SERIES]*`},
                                    {"type": "plain_text", "text": "List all current matches"},

                                    {"type": "mrkdwn", "text": `*${slashCommand} score ID*`},
                                    {"type": "plain_text", "text": "Score details for match ID"},
                                ]
                            },
                        ]
                    });
                break;
        }
    });

(async () => {
    await app.start(process.env.PORT || 8080);
    app.receiver.app.get("/healthcheck", (req, res) => res.sendStatus(200));
    console.log('⚡️ Cricket Bot is running!');
})();