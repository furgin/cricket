const axios = require('axios')
    , {DateTime} = require('luxon')
    , cheerio = require('cheerio');


const cache = {};

const load = (key, expiry, fn) => {
    const cacheMiss = !cache[key], cacheExpiry = cache[key] && cache[key].expiry_time < DateTime.local();
    if (cacheMiss || cacheExpiry) {
        //if (cacheMiss) console.log("cache miss: " + key);
        //if (cacheExpiry) console.log("cache expiry: " + key);
        return fn().then(data => {
            cache[key] = {
                expiry_time: expiry,
                data: data
            };
            return data;
        })
    } else {
        //console.log("cache hit: "+key);
        return Promise.resolve(cache[key].data);
    }
};

module.exports = {

    matches: () => {
        return load("matches",
            DateTime.local().plus({minutes: 10}),
            () => axios.get("https://www.espncricinfo.com/ci/engine/match/index.html",
                {
                    params: {view: "week"}
                })
                .then((resp) => cheerio.load(resp.data))
                .then($ => $('a')
                    .toArray()
                    .filter(link => $(link).text() === "Scorecard")
                    .map((link) => $(link).attr("href"))
                    .map(link => link.split('/'))
                    .map(parts => {
                        return {series_id: parseInt(parts[4]), unique_id: parseInt(parts[6])}
                    })
                )
        );
    },

    match: (series_id, unique_id) => {

        return load(`match-${unique_id}`,
            DateTime.local().plus({minutes: 1}),
            () => axios.get(`https://www.espncricinfo.com/matches/engine/match/${unique_id}.json`)
                .then(resp => resp.data)
                .then(match => {

                    const teams = [
                        {
                            team_id: match.match.team1_id,
                            object_id: match.match.team1_object_id,
                            name: match.match.team1_name,
                            short_name: match.match.team1_short_name,
                            abbreviation: match.match.team1_abbreviation,
                            logo: `https://img1.hscicdn.com/image/upload/f_auto,t_s_100/esci/i/teamlogos/cricket/500/${match.match.team1_object_id}.png`
                        },
                        {
                            team_id: match.match.team2_id,
                            object_id: match.match.team2_object_id,
                            name: match.match.team2_name,
                            short_name: match.match.team2_short_name,
                            abbreviation: match.match.team2_abbreviation,
                            logo: `https://img1.hscicdn.com/image/upload/f_auto,t_s_100/esci/i/teamlogos/cricket/500/${match.match.team2_object_id}.png`
                        }
                    ];

                    const result = {
                        title: match.match.match_status === "complete" ? "RESULT" : "SCHEDULE",
                        series_id: series_id,
                        unique_id: unique_id,
                        description: `${match.match.team1_name} vs ${match.match.team2_name}`,
                        short_description: `${match.match.team1_short_name} vs ${match.match.team2_short_name}`,
                        innings: match.innings.map(inn => {
                            return {
                                batting_team: teams.find(team => team.team_id === inn.batting_team_id),
                                innings_number: parseInt(inn.innings_number),
                                innings_status: inn.event_name,
                                wickets: parseInt(inn.wickets),
                                runs: parseInt(inn.runs),
                                target: (inn.target && inn.target > 0) ? parseInt(inn.target) : null,
                                over_limit: inn.over_limit,
                                overs: inn.overs,
                                description: (inn.target && inn.target > 0) ? `(${inn.over_limit} ov, target ${inn.target}) ${inn.runs}/${inn.wickets}` : `${inn.runs}/${inn.wickets}`,
                                score_description: `${inn.runs}/${inn.wickets}`
                            }
                        }),
                        match_status: match.match.match_status,
                        winner_team_id: match.match.tiebreaker_name ? match.match.tiebreaker_team_id : match.match.winner_team_id,
                        start_datetime_gmt: DateTime.fromFormat(match.match.start_datetime_gmt + " +0", "yyyy-MM-dd HH:mm:ss Z")
                    };

                    result.winner_team = teams.find(team => team.team_id === result.winner_team_id);

                    switch (match.match.result_name) {
                        case "match tied":
                            result.result_description = `${match.match.result_name} (${result.winner_team.name} won on ${match.match.tiebreaker_name}.)`;
                            break;
                        case "won":
                            const first_innings = result.innings.find(inn => inn.innings_number === 1);
                            const team_batted_first = teams.find(team => team.team_id === first_innings.batting_team.team_id);
                            const winning_innings = match.innings.filter(inn => inn.batting_team_id === result.winner_team_id);
                            const losing_innings = match.innings.filter(inn => inn.batting_team_id !== result.winner_team_id);

                            if (team_batted_first.team_id === result.winner_team_id) {
                                const winning_runs = winning_innings.reduce((acc, cur) => acc + cur.runs, 0)
                                    , losing_runs = losing_innings.reduce((acc, cur) => acc + cur.runs, 0)
                                    , runs_description = `${winning_runs - losing_runs} runs.`;
                                result.result_description = `${result.winner_team.name} won by ${runs_description}`;
                            } else {
                                const last_innings = result.innings.reduce((acc, cur) => {
                                    if (cur.innings_number > acc.innings_number) {
                                        return cur;
                                    }
                                    return acc;
                                }, {innings_number: -1});

                                result.result_description = `${result.winner_team.name} won by ${10 - last_innings.wickets} wickets.`;
                            }
                            break;
                        case "match abandoned without a ball bowled":
                            result.result_description = "match abandoned without a ball bowled";
                            break;
                    }

                    result.teams = teams.map(team => {
                        team.innings = result.innings.filter(inn => inn.batting_team.team_id === team.team_id);
                        team.score_description = team.innings.map((inn) => inn.score_description).join("&");
                        return team;
                    });

                    let series = match.series.find(series => parseInt(series.core_recreation_id) === series_id);
                    if (!series) {
                        series = match.series[match.series.length - 1];
                    }
                    if (series) {
                        result.series = {
                            name: series.series_name,
                            short_name: series.series_short_name ? series.series_short_name : series.series_name
                        };
                    } else {
                        result.series = {
                            name: "Unknown",
                            short_name: "Unknown"
                        };
                    }

                    return result;
                }));

    }

};