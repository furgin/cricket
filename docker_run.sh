#!/usr/bin/env bash

. .env

docker stop "cricket-bot"
docker rm "cricket-bot"

docker run -ti --rm \
  -p 8080:8080 \
  --name "cricket-bot" \
  -e SLACK_SIGNING_SECRET="${SLACK_SIGNING_SECRET}" \
  -e SLACK_BOT_TOKEN="${SLACK_BOT_TOKEN}" \
  -e USER="${USER}" \
  "docker.atl-paas.net/${USER}/cricket-bot"
